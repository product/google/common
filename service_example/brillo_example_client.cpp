/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <iostream>
#include <string>

#include <base/at_exit.h>
#include <base/bind.h>
#include <base/logging.h>
#include <base/message_loop/message_loop.h>
#include <binder/IInterface.h>
#include <binder/IServiceManager.h>
#include <brillo/binder_watcher.h>
#include <brillo/message_loops/base_message_loop.h>
#include <brillo/syslog_logging.h>
#include <utils/String16.h>

#include "android/brillo/example/BnAlertCallback.h"
#include "android/brillo/example/IExampleService.h"

namespace android {

class AlertCallback : public android::brillo::example::BnAlertCallback {
 public:
  binder::Status OnNLogEventsReceivedCallback(int n) {
    LOG(INFO) << "Received Callback from service: Received " << n << " log "
              << "messages.";
    return binder::Status::ok();
  }
};

}  // namespace android

// A function to read user input on the console and pass it along to the example
// service for logging.
void StdinCallback(
    android::sp<android::brillo::example::IExampleService> service) {
  std::string line;
  std::getline(std::cin, line);
  service->Log(android::String16(line.c_str()));
}

int main() {
  // TODO(ralphnathan): Hide this in brillo::BaseMessageLoop.
  base::AtExitManager at_exit_manager;
  brillo::InitLog(brillo::kLogToSyslog);
  // Get handle for the example service.
  android::sp<android::brillo::example::IExampleService> service;
  android::status_t status = android::getService(
      android::String16("android.brillo.example.ExampleService"), &service);
  CHECK(status == android::OK) <<
      "Failed to get IExampleService binder from service manager!";

  // Register a callback object with the service.
  android::sp<android::AlertCallback> cbo = new android::AlertCallback();
  service->RegisterCallback(cbo, 3);

  // Create a message loop.
  base::MessageLoopForIO message_loop_for_io;
  brillo::BaseMessageLoop message_loop{&message_loop_for_io};

  // Initialize a binder watcher.
  brillo::BinderWatcher watcher(&message_loop);
  watcher.Init();

  // Poll stdin.
  base::Closure stdin_callback = base::Bind(&StdinCallback, service);
  message_loop.WatchFileDescriptor(
      STDIN_FILENO, brillo::BaseMessageLoop::kWatchRead, true, stdin_callback);
  // Start the message loop.
  message_loop.Run();
}
